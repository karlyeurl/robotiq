#include "SModelHandAPI.h"

/* TODO reomve global ugly stuff kludges */
bool _hapi_status_rcv = false;
robotiq_s_model_control::SModel_robot_input status;

void handStatusCallback(const robotiq_s_model_control::SModel_robot_input msg) {
   status = msg;
   _hapi_status_rcv = true;
}

Hand::Hand(){
   int argc = 0;
   char **argv = NULL;
   ros::init(argc, argv, "SModelHandAPI");
   ros::NodeHandle n;

   hand_sub = n.subscribe("SModelRobotInput", 1000, handStatusCallback);
   /* Initializes status */
   while(!_hapi_status_rcv) {
      ros::spinOnce();
   }

   hand_pub = n.advertise<robotiq_s_model_control::SModel_robot_output>("SModelRobotOutput", 1000);
   robotiq_s_model_control::SModel_robot_output command = Hand::newCmd();
   Hand::send(10, 3, command);
}

Hand::Hand(bool activation){
   int argc = 0;
   char **argv = NULL;
   ros::init(argc, argv, "SModelHandAPI");
   ros::NodeHandle n;

   hand_sub = n.subscribe("SModelRobotInput", 1000, handStatusCallback);
   /* Initializes status */
   while(!_hapi_status_rcv) {
      ros::spinOnce();
   }

   hand_pub = n.advertise<robotiq_s_model_control::SModel_robot_output>("SModelRobotOutput", 1000);
   if (activation) {
      robotiq_s_model_control::SModel_robot_output command = Hand::newCmd();
      Hand::send(10, 3, command);
   }
}

robotiq_s_model_control::SModel_robot_output Hand::newCmd(){
   robotiq_s_model_control::SModel_robot_output command;
   command.rACT = 1;
   command.rGTO = 1;
   command.rSPA = 255;
   command.rSPB = 255;
   command.rSPC = 255;
   command.rFRA = 150;
   command.rFRB = 150;
   command.rFRC = 150;
   return command;
}

/**
 * Force control.
 **/
void Hand::setForce(robotiq_s_model_control::SModel_robot_output* command, unsigned char force){
   command->rFRA = force;
   command->rFRB = force;
   command->rFRC = force;
}

void Hand::setForce(robotiq_s_model_control::SModel_robot_output* command, unsigned char force1, unsigned char force2, unsigned char force3){
   command->rFRA = force1;
   command->rFRB = force2;
   command->rFRC = force3;
}

/**
 * Speed control.
 **/
void Hand::setSpeed(robotiq_s_model_control::SModel_robot_output* command, unsigned char speed){
   command->rSPA = speed;
   command->rSPB = speed;
   command->rSPC = speed;
}

void Hand::setSpeed(robotiq_s_model_control::SModel_robot_output* command, unsigned char speed1, unsigned char speed2, unsigned char speed3){
   command->rSPA = speed1;
   command->rSPB = speed2;
   command->rSPC = speed3;
}

/**
 * Position control.
 **/
void Hand::setGoal(robotiq_s_model_control::SModel_robot_output* command, unsigned char goal){
   command->rICF = 0;
   command->rPRA = goal;
}

void Hand::setGoal(robotiq_s_model_control::SModel_robot_output* command, unsigned char finger1,unsigned char finger2, unsigned char finger3){
   command->rICF = 1;
   command->rPRA = finger1;
   command->rPRB = finger2;
   command->rPRC = finger3;
}

/* Opens all the fingers */
void Hand::openCmd(robotiq_s_model_control::SModel_robot_output* command){
   command->rICF = 0;
   command->rPRA = 0;
}

/* Closes all the fingers */
void Hand::closeCmd(robotiq_s_model_control::SModel_robot_output* command){
   command->rICF = 0;
   command->rICS = 0;
   command->rPRA = 255;
}

void Hand::modeCmd(robotiq_s_model_control::SModel_robot_output* command, unsigned char mode){
   if (mode < 5){
      command->rICS = 0;
      command->rMOD = mode;
   }
}

void Hand::scissorsCmd(robotiq_s_model_control::SModel_robot_output* command, unsigned char axis_pos, unsigned char axis_speed, unsigned char axis_force){
   command->rICS = 1;
   command->rMOD = SCISSOR_MODE;
   command->rPRS = axis_pos;
   command->rSPS = axis_speed;
   command->rFRS = axis_force;
}

void Hand::stopCmd(robotiq_s_model_control::SModel_robot_output* command){
   command->rGTO = 0;
}

void Hand::restartCmd(robotiq_s_model_control::SModel_robot_output* command){
   command->rGTO = 1;
}

void Hand::autorelease(){
   robotiq_s_model_control::SModel_robot_output command;
   command.rATR = 1;
   Hand::send(10, 3, command);
}

void Hand::reset(){
   robotiq_s_model_control::SModel_robot_output command;
   command.rACT = 0;
   Hand::send(10, 3, command);
}

void Hand::send(int rate, int times, robotiq_s_model_control::SModel_robot_output command){
   ros::Rate loop_rate(rate);
   int count = 0;
   ROS_INFO("sending %d times the command: [ACT=%d MOD=%d GTO=%d ATR=%d GLV=%d ICF=%d ICS=%d PRA=%d SPA=%d FRA=%d PRB=%d SPB=%d FRB=%d PRC=%d SPC=%d FRC=%d PRS=%d SPS=%d FRS=%d].", times, command.rACT, command.rMOD, command.rGTO, command.rATR, command.rGLV, command.rICF, command.rICS, command.rPRA, command.rSPA, command.rFRA, command.rPRB, command.rSPB, command.rFRB, command.rPRC, command.rSPC, command.rFRC, command.rPRS, command.rSPS, command.rFRS);
   while (count < times){
      hand_pub.publish(command);
      ros::spinOnce();
      loop_rate.sleep();
      count++;
   }
}

robotiq_s_model_control::SModel_robot_input Hand::getStatus(){
   _hapi_status_rcv = false;
   while (!_hapi_status_rcv) {
      ros::spinOnce();
   }
   //std::cout << status << std::endl;
   return status;
   //test = 1;
   //int argc = 0;
   //char **argv = NULL;
   //ros::init(argc, argv, "SModelHandListener");
   //ros::NodeHandle k;
   //std::cout << "test before = " << test << std::endl;
   //ros::Subscriber sub = n.subscribe("SModelRobotInput", 1000, mystatusCallback);
   //ros::spinOnce();
   //std::cout << "test after = " << test << std::endl;
   //statusCallback();
}

Hand::~Hand(){
}
