#include "ros/ros.h"
#include "robotiq_s_model_control/SModel_robot_output.h"
#include "robotiq_s_model_control/SModel_robot_input.h"
#define BASIC_MODE 0
#define PINCH_MODE 1
#define WIDE_MODE 2
#define SCISSOR_MODE 3

/** This API is very open by design. One could want to forge their own command, bypassing all the methods setting the positions. **/

class Hand {
   public:
   Hand();
   Hand(bool activation);
   robotiq_s_model_control::SModel_robot_output newCmd();
   void setForce(robotiq_s_model_control::SModel_robot_output* command, unsigned char force);
   void setForce(robotiq_s_model_control::SModel_robot_output* command, unsigned char force1, unsigned char force2, unsigned char force3);
   void setSpeed(robotiq_s_model_control::SModel_robot_output* command, unsigned char speed);
   void setSpeed(robotiq_s_model_control::SModel_robot_output* command, unsigned char speed1, unsigned char speed2, unsigned char speed3);
   void setGoal(robotiq_s_model_control::SModel_robot_output* command, unsigned char goal);
   void setGoal(robotiq_s_model_control::SModel_robot_output* command, unsigned char finger1, unsigned char finger2, unsigned char finger3);
   void openCmd(robotiq_s_model_control::SModel_robot_output* command);
   void closeCmd(robotiq_s_model_control::SModel_robot_output* command);
   void modeCmd(robotiq_s_model_control::SModel_robot_output* command, unsigned char mode);
   void scissorsCmd(robotiq_s_model_control::SModel_robot_output* command, unsigned char axis_pos, unsigned char axis_speed, unsigned char axis_force);
   void stopCmd(robotiq_s_model_control::SModel_robot_output* command);
   void restartCmd(robotiq_s_model_control::SModel_robot_output* command);
   void autorelease();
   void reset();
   void send(int rate, int times, robotiq_s_model_control::SModel_robot_output command);
   robotiq_s_model_control::SModel_robot_input getStatus();
   ~Hand();

   private:
   ros::Publisher hand_pub;
   ros::Subscriber hand_sub;
};
